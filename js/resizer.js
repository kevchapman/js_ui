function Resizer(opts){
	this.b = $('body');
	this.w = this.b.width();
	this.functions = [];
	this.win = $(window);
	this.h = this.win.height();
	(function(t){ t.init(); })(this);
}
Resizer.prototype = {
	init:function(){
		var t = this;
		this.win.on('resize.resizer',function(){
			var w = t.b.width(), h = t.win.height();
			if(w !== t.w || h !== t.h){
				t.run(w);
				t.w = w;
				t.h = h;
			}
		})
	},
	add:function(f){
		this.functions.push(f);
	},
	run:function(w){
		var t = this;
		t.timer ? clearTimeout(t.timer) : '';
		t.timer = setTimeout(function(){ 
			var i, l = t.functions.length;
			for(i=0; i<l; i++){ t.functions[i](); }
		},300);
	}
}