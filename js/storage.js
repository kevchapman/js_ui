// Storage
function Cookie(opts){
	this.exd = opts.exdays || 365;
}
Cookie.prototype = {
	getItem: function(name){
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++) {
		  x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
		  y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
		  x=x.replace(/^\s+|\s+$/g,"");
		  if (x==name) { return unescape(y); }
		}
	},
	setItem:function(name,val,exd){
		var exdays = exd || null;
		var exdate = new Date();
		exdate.setDate(exdate.getDate() + exdays);
		var c_val = escape(val) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
		document.cookie=name + "=" + c_val;
	},
	removeItem:function(name){
		document.cookie = name +'=; expires=Thu, 01-Jan-70 00:00:01 GMT;';
	}
}

function Lm_storage(type){
	sType = type && type === 'session' ? 'sessionStorage' : 'localStorage';
	this.s = window[sType] || new Cookie({exdays: sType === 'sessionStorage' ? 1 : false});
	this.allow = this.s.getItem('allow') || false;
}
Lm_storage.prototype = {
	init:function(){
		var t = this;
	},
	getItem:function(key){
		return this.s.getItem(key);
	},
	setItem:function(key,val,exdays){
		this.s.setItem(key,val,exdays);
	},
	removeItem:function(key){
		this.s.removeItem(key);
		if(key === 'allow'){ this.allow = false; }
	}
}