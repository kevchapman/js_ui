Array.prototype.contains = function(str) {
	var i = this.length;
	while (i--) {
		if (this[i] === str) {return true; } 
	}
	return false;
}
Array.prototype.remove = function(str){
	var l = this.length, i;
	for(i=0; i<l; i++){
		if(this[i] === str){
			this.splice(i,1);
			break;
		}
	}
}
Array.prototype.toPipes = function(){
	var l = this.length, i, p, str = '';
	for(i=0; i<l; i++){
		p = i ? '|' : '';
		str+=p+this[i];
	}
	return str;
}