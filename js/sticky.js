function Sticky(ele,opts){
	var defaults = {
		wrap: false,
		over:false
	}
	var o = $.extend({},defaults,opts);
	if(!ele.length || !o.wrap){ return; }
	this.opts = o;
	this.ele = ele;
	this.wrap = o.wrap.addClass('sticky');
	this.win = $(window);
	this.init();
}
Sticky.prototype = {
	init:function(){
		var t = this;
		this.setProps();
		var viewport = this.win.height();
		if(viewport > this.h && (this.wrap.height()) > this.h){
			this.win.on('scroll.sticky',function(){ t.scroll(this); });
		}
	},
	setProps:function(){
		//this.wrap.height('auto').height( this.wrap.height() );
		this.ele.width('auto').width(this.ele.parent().width()).height('auto').height( this.ele.height() );
		this.y = this.ele.offset().top;
		this.maxH = this.wrap.height() + this.y;
		this.h = this.ele.outerHeight(true);
	},
	scroll:function(ele){
		var st = $(ele).scrollTop();
		var fixed = st >= this.y;
		var contain = this.h+st >= this.maxH;
		this.ele[ fixed ? 'addClass' : 'removeClass' ]('fixed');
		if(fixed && this.opts.over && !this.hasPad){
			this.wrap.css({'padding-top': '+='+this.h});
			this.hasPad = true;
		} else if(!fixed && this.opts.over && this.hasPad) {
			this.wrap.css({'padding-top':'-='+this.h});
			this.hasPad = false;
		}
		this.ele[contain ? 'addClass' : 'removeClass']('contain');
	},
	reInit:function(){
		this.ele.removeClass('contain fixed');
		if(this.hasPad){this.wrap.css({'padding-top':'-='+this.h})}
		this.win.unbind('scroll.sticky');
		this.init();
		this.scroll(this.win);
	}
}
!function($){
	$.fn.sticky = function(opts){
		return this.each(function(){
			$(this).data().sticky = new Sticky($(this),opts);
		});
	}
}(jQuery)