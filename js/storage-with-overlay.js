function Cookie(opts){}
Cookie.prototype = {
	getItem: function(name){
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++) {
		  x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
		  y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
		  x=x.replace(/^\s+|\s+$/g,"");
		  if (x==name) { return unescape(y); }
		}
	},
	setItem:function(name,val,exd){
		var exdays = exd || 365;
		var exdate = new Date();
		exdate.setDate(exdate.getDate() + exdays);
		var c_val = escape(val) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
		document.cookie=name + "=" + c_val;
	},
	removeItem:function(name){
		document.cookie = name +'=; expires=Thu, 01-Jan-70 00:00:01 GMT;';
	}
}

function Cookie_modal(opts){
	this.speed = opts.speed || 400;
	this.modal = opts.wrap || $('#cookie_modal');
	this.btn_allow = opts.btn_allow || $('#cookie_modal_allow');
	this.btn_hide = opts.btn_hide || $('#cookie_modal_hide');
	this.controller = opts.controller;
	this.allow = false;
	(function(t){ t.init(); })(this)
}
Cookie_modal.prototype = {
	init:function(){
		var t = this;
		this.btn_hide.on('click',function(){ t.close(); });
	},
	open:function(callback){
		var t = this, cb = callback || function(){};
		this.modal.slideDown(t.speed);
		if(this.controller){
			this.btn_allow.unbind().on('click',function(){ t.controller.allow = true; t.controller.setItem('allow','true'); cb(); t.close(); });
		}
	},
	close: function(){
		var t = this;
		this.modal.slideUp(t.speed);
	}
}

function Lm_storage(){
	this.s = window.localStorage || new Cookie();
	this.allow = this.s.getItem('allow') || false;
	this.modal = new Cookie_modal({wrap:$('#cookie_modal'),controller:this});
	(function(t){ t.init(); })(this);
}
Lm_storage.prototype = {
	init:function(){
		var t = this;
		window.onload = function(){ t.allow ? '' : t.modal.open(); }
	},
	getItem:function(key){
		return this.s.getItem(key);
	},
	setItem:function(key,val,exdays){
		var t = this;
		if(this.allow){
			this.s.setItem(key,val,exdays);
		} else {
			var cb = function(){ t.s.setItem(key,val,exdays); }
			this.modal.open(cb);
		}
	},
	removeItem:function(key){
		this.s.removeItem(key);
		if(key === 'allow'){ this.allow = false; }
	}
}

$().ready(function(){ window.storage = new Lm_storage(); });