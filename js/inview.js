function NamedEventStack(){
	this.events = [];
}
NamedEventStack.prototype = {
	add:function(name,f){
		this.events[name] = f;
	},
	run:function(){
		var l = this.events.length, i;
		for(key in this.events){
			this.events[key]();
		}
	}
}

function InviewController(){
	var t = this;
	this.win = $(window);
	this.h = this.win.height();
	this.vis =this.win.height()+this.win.scrollTop();
	this.eventstack = new NamedEventStack();
	this.win.off('scroll.inview').on('scroll.inview',function(){
		t.checkPositions();
	});
	core.resizer.add(function(){
		t.setHeight();
		t.checkPositions();
	})
	this.timer = false;
	core.loadEvents.add(function(){ t.win.scroll(); });
}
InviewController.prototype = {
	checkPositions:function(){
		var t = this;
		var st = this.win.scrollTop()
		this.vis = this.h+st;
		t.eventstack.run();
	},
	setHeight:function(){
		this.h = this.win.height();
		console.log(this.h);
	}
}

function Inview(ele,func){
	var t = this;
	this.ele = ele;
	this.ele.data().visible = false;
	if(!window.inviewController){ window.inviewController = new InviewController(); }
	inviewController.eventstack.add(ele[0].id,function(){
		if((ele.offset().top + ele.outerHeight()) < inviewController.vis && !ele.data().visible){
			func();
			ele.data().visible = true;
			inviewController.eventstack.events[ele[0].id] = function(){};
		}
	});
	$(window).scroll();
}
Inview.prototype = {}

!function($){
	$.fn.inview = function(opts){
		return this.each(function(){
			$(this).data().inview = new Inview($(this),opts);
		});
	}
}(jQuery);