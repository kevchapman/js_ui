// requires jquery, Modernizr

function FadeGallery(opts){
	this.opts = opts || false;
	this.wrap = opts.wrap;
	this.controller = opts.controller || false;
	this.cb = opts.cb || function(){};
	this.index = 0;
	this.css3 = Modernizr.csstransitions && Modernizr.opacity ? true : false;
	this.speed = opts.speed || 1500;
	this.bg = opts.bg || false;
	//this.bgi = opts.bgi || false;
	return this;
}
FadeGallery.prototype = {
	init:function(){
		var t = this;
		// add loader
		this.wrap.prepend('<div class="loading" style="display:none"><span>Loading</span></div>');
		this.panels = this.wrap.find('div.panel');
		if(this.opts.oh && this.opts.ow){
			this.ratio = this.opts.ow/this.opts.oh;
			this.wrap.add(this.panels).height(this.wrap.width()/this.ratio);
		}
		if(this.css3){
			this.panels.each(function(i){
				this.style[Modernizr.prefixed('transition')] = 'opacity '+t.speed+'ms ease';
			}).not(':first').css({opacity:0});
		} else {
			this.panels.not(':first').hide();
		}
		this.panels.first().addClass('active');
		this.images = this.opts.imagePaths;
		this.current = this.images[0];
		this.total =this.images.length;
		this.opts = null;
		return this;
	},
	goTo:function(n,path){
		var url = path || this.images[n];
		if(!url){return false}
		if(url === this.current){ this.cb(); return; }
		this.wrap.addClass('loading');
		var t = this,
			a = this.panels.filter('.active'),
			img = new Image();
	
		img.onload = function(){
			t.fade(a,0);
			_in(a);
		} 
		img.src = url;
		function _in(panel){
			panel.removeClass('active');
			var np = t.panels.not(panel);
			t.bg ? np.css({backgroundImage:'url('+url+')'}) : np.html(img);
			t.wrap.removeClass('loading');
			t.fade(np.addClass('active'),1)
			if(n || n === 0){t.index = n}
			t.current = url;
			t.cb();	
		}
	},
	next:function(){
		this.index++;
		if(this.index >= this.total){ this.index = 0 }
		this.goTo(this.index);
	},
	prev:function(){
		this.index--;
		if(this.index <= 0){ this.index = this.total-1 }
		this.goTo(this.index);
	},
	fade:function(ele,n,cb){
		if(this.css3){
			ele.unbind().bind(transEndEventName,function(e){
				cb ? cb() : '';
			})
			ele.css({opacity:n});
		} else {
			ele.stop(true).fadeTo(this.speed,n,function(){ cb ? cb() : ''; })	
		}
	},
	addItem:function(array,go){
		var t = this;
		for(var i=0; i<array.length; i++){
			this.images.unshift(array[i]);
		}
		this.total = this.images.length;
		this.index = (this.index+array.length);
		go ? this.goTo(0) : '';
	},
	replace:function(url){
		this.goTo(0,url);
	}
}