function EventStack(){
	this.events = [];
}
EventStack.prototype = {
	add:function(f){
		this.events.push(f);
	},
	run:function(){
		var l = this.events.length, i;
		for(i=0; i<l; i++){
			this.events[i]();
		}
	}
}