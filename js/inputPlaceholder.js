function InputPlaceholder(ele){
	if(!ele){ return; }
	this.ele = ele;
	(function(t){ t.init(); })(this);
}
InputPlaceholder.prototype = {
	init:function(){
		//if('placeholder' in this.ele[0]){ return; }
		if(Modernizr.input.placeholder){ return; }
		var t = this;
		this.pl = this.ele.attr('placeholder') || 'placeholder';
		this.ele.val() === '' ? this.ele.val(this.pl).addClass('placeholder') : '';
		this.ele.on({
			focus:function(){ t.ele.val() === t.pl ? t.ele.val('').removeClass('placeholder') : '' }
			,blur:function(){ t.ele.val() === '' ? t.ele.val(t.pl).addClass('placeholder') : '' }
		})
		this.ele.parents('form').submit(function(){
			t.ele.val() === t.pl ? t.ele.val('') : '';
		})
	}
}